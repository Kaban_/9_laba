#include <locale.h>
#include <stdio.h>
#include "Header.h"
#define SIZE 5

float input(int pol_1[][SIZE], int pol_2[][SIZE]) {
	for (int i = 0; i != SIZE; i++) {
		printf("%d коэффицент первого многочлена: ", i + 1);
		if (scanf("%d", &pol_1[1][i]) == NULL) return 0.5;
	}
	for (int i = 0; i != SIZE; i++) {
		printf("%dя степень первого многочлена: ", i + 1);
		if (scanf("%d", &pol_1[0][i]) == NULL) return 0.5;
	}
	for (int i = 0; i != SIZE; i++) {
		printf("%d коэффицент второго многочлена: ", i + 1);
		if (scanf("%d", &pol_2[1][i]) == NULL) return 0.5;
	}
	for (int i = 0; i != SIZE; i++) {
		printf("%dя степень второго многочлена: ", i + 1);
		if (scanf("%d", &pol_2[0][i]) == NULL) return 0.5;
	}
	return 0;
}

void proiz(int s[][SIZE]) {
	for (int i = 0; i < SIZE; i++) {
		s[1][i] = s[0][i] * s[1][i];
		s[0][i] -= 1;
	}
}

void umn(int rez[2][SIZE * SIZE], int pol_1[][SIZE], int pol_2[][SIZE]) {
	int h = 0;
	for (int j = 0; j < SIZE; j++) {
		for (int i = 0; i < SIZE; i++) {
			rez[0][h] = pol_1[0][j] + pol_2[0][i];
			h++;
		}
	} h = 0;
	for (int j = 0; j < SIZE; j++) {
		for (int i = 0; i < SIZE; i++) {
			rez[1][h] = pol_1[1][j] * pol_2[1][i];
			h++;
		}
	}
}

void result(int rez[][SIZE * SIZE]) {
	int i = 0, count = 0;
	int min = rez[0][i]; i++;
	while (i < SIZE * SIZE) {
		if (rez[0][i] < min) {
			min = rez[0][i];
			i++;
		}
		else i++;
	} i = 0;
	int max = rez[0][i]; i++;
	while (i < SIZE * SIZE) {
		if (rez[0][i] > max) {
			max = rez[0][i];
			i++;
		}
		else i++;
	}i = 0;
	printf("\n");
	while (max >= min) {
		while (i < SIZE * SIZE) {
			if (rez[0][i] == max) {
				count += rez[1][i];
				i++;
			}
			else i++;
		}
		if (count != 0)
			printf("%dX^%d  ", count, max);
		max--;
		count = 0;
		i = 0;
	}
}