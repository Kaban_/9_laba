#include <locale.h>
#include <stdio.h>
#include "Header.h"
#define SIZE 5


int main() {
	setlocale(LC_ALL, "Rus");
	float x = 0;
	int pol_1[2][SIZE], pol_2[2][SIZE], rez[2][SIZE * SIZE];
	if ((x = input(pol_1, pol_2)) == 0.5) return 0;
	proiz(pol_1);
	proiz(pol_2);
	umn(rez, pol_1, pol_2);			
	result(rez);
	return 0;
}